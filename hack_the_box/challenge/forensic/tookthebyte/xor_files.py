#!/usr/bin/env python3

import os

file_to_xor = 'my_file'
for a in range (0x00,0x100):
	b = bytearray(open(file_to_xor, 'rb').read())
	for i in range(len(b)):
	    b[i] ^= a 
	open('out_%i'%a, 'wb').write(b)
	print('result of XOR with byte %x' %a)

	cmd = 'hexdump -C b.out_%i'%a
	os.system(cmd)


