#This scripts retrives a captcha image from a web page, 
#extract the string and submit it back.

import requests
import urllib
import re
import base64
import os
import subprocess
import cv2
import numpy as np

#send a get request to the url
success = False
post_response = 'retente ta chance'
while success != True :
   target_url = "http://challenge01.root-me.org/programmation/ch7/"
   session = requests.Session()
   response = session.get(target_url)
   page = response.text
   #print(page)
   m = re.search(r'(?:<img src="data:image\/png;)(.*?)(" \/>)',page)
   qr_bin = base64.b64decode(m[1].replace('base64,',''))
   print('\n\n\n\n')
   file_name = 'qr.png'
   fd = open(file_name,'wb')
   fd.write(qr_bin)
   fd.close()

   imagename = "qr.png"
   img = cv2.imread(imagename, 0) # 0 params, for gray image
   height, width = img.shape[:2]  # image height and width

   #modify the QR code so it is readable
   for y in range(height):
       for x in range(width):
          # if y == 17: img[x,y]=0
          # if y == 26: img[x,y]=0
          # if y == 35: img[x,y]=0
          # if y == 44: img[x,y]=0
          # if y == 53: img[x,y]=0
          # if y == 62: img[x,y]=0
          # if y == 71: img[x,y]=0
          # if y == 81: img[x,y]=0
          # if x == 17: img[x,y]=0
          # if x == 26: img[x,y]=0
          # if x == 35: img[x,y]=0
          # if x == 44: img[x,y]=0
          # if x == 53: img[x,y]=0
          # if x == 62: img[x,y]=0
          # if x == 71: img[x,y]=0
          # if x == 80: img[x,y]=0
   
          # if y == 218: img[x,y]=0
          # if y == 227: img[x,y]=0
          # if y == 236: img[x,y]=0
          # if y == 263: img[x,y]=0
          # if y == 272: img[x,y]=0
          # if y == 281: img[x,y]=0
   
          # if x == 218: img[x,y]=0
          # if x == 227: img[x,y]=0
          # if x == 236: img[x,y]=0
          # if x == 263: img[x,y]=0
          # if x == 272: img[x,y]=0
          # if x == 281: img[x,y]=0
   
           #fill upper left corner
           if 26>y>17 and 17< x<82:
               img[y,x]=0
           if 81>y>17 and 17< x<26:
               img[y,x]=0
           if 81>y>71 and 17< x<82:
               img[y,x]=0
           if 81>y>17 and 71< x<82:
               img[y,x]=0
           if 62>y>35 and 35< x<62:
               img[y,x]=0

           #fill lower left corner
           if 281>y>272 and 17<x<82:
               img[y,x]=0
           if 281>y>218 and 17<x<27:
               img[y,x]=0
           if 227>y>218 and 17<x<82:
               img[y,x]=0
           if 281>y>218 and 71<x<82:
               img[y,x]=0
           if 263>y>236 and 35<x<62:
               img[y,x]=0

           #fill upper right corner
           if 281>=x>=272 and 17< y<81:
               img[y,x]=0
           if 281>=x>=218 and 17< y<26:
               img[y,x]=0
           if 227>=x>=218 and 17< y<81:
               img[y,x]=0
           if 281>x>218 and 71< y<81:
               img[y,x]=0
           if 263>x>236 and 35<y<62:
               img[y,x]=0
   
   #saving the new qr-code
   cv2.imwrite('modified_qr.png',img)
   qr_string = subprocess.check_output('zbarimg modified_qr.png', shell=True)
   qr_string = qr_string.decode()
   if 'The key is' in qr_string:
       qr_key = re.search(r'(?:is\s)(.*)',qr_string)
       qr_key = qr_key[1]
       print(qr_key)
        
    
   #submitting to the form
   post_tag ={'metu':qr_key} 
   #post_tag = urllib.urlencode(post_tag)
   print('submitting the following data:')
   print(post_tag)
   print('\n\n')
   
   post_response = session.post(target_url, data=post_tag)
   if 'retente' in post_response.text:
       print(post_response.text)
       print('bouuuuuuhhhh')
   else:
       success = True
       print(post_response.text)
       print('YEAAAAHHHHHH')



