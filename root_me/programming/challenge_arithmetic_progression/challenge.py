#This scripts retrieves the web page source code
#extract the values of the progression from the source code
#computes the progression
#and submits the result

import urllib
import re
import requests

def progression_un1(n, un, shift, factor):
    '''function that defines the progression'''
    return (shift + un) + (n*factor)

#retrieving the web page source code
target_url = "http://challenge01.root-me.org/programmation/ch1/"
session = requests.Session()
response = session.get(target_url)
page = response.text
print(page)

#using regexp to extract all the useful parameters
shift = re.search(r'(?:=\s\[\s)([0-9-][0-9]*)',page)
shift = int(shift[1])
print('shift: ', shift)

factor = re.search(r'(?:\[\sn\s\*\s)([0-9-][0-9]*)',page)
factor = int(factor[1])
print('factor: ', factor)

un = re.search(r'(?:sub>\s=\s)([0-9-][0-9]*)',page)
un = int(un[1])
print('u0: ', un)

n_max = re.search(r'(?:U<sub>)([0-9-][0-9]*)(<\/sub><br)',page)
n_max = int(n_max[1])
print('n_max: ',n_max)

#looping to go through the progression
for i in range(1,n_max+1):
    u_n1 = progression_un1(i-1,un, shift, factor)
    un = u_n1

#sending the result to the page
response = session.get("http://challenge01.root-me.org/programmation/ch1/ep1_v.php?result=%i"%un)

print(response.text)
