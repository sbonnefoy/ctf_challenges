#This scripts retrives a captcha image from a web page, 
#extract the string and submit it back.

import requests
import urllib
import re
import base64
import os
import subprocess

#send a get request to the url
success = False
post_response = 'retente ta chance'
while success != True :
    target_url = "http://challenge01.root-me.org/programmation/ch8/"
    session = requests.Session()
    response = session.get(target_url)
    page = response.text
#    print(page)
    m = re.search(r'(?:<img src="data:image\/png;)(.*?)(" \/>)',page)
    captcha_bin = base64.b64decode(m[1].replace('base64,',''))
#    print(captcha_bin)
    print('\n\n\n\n')
    fd = open('captcha_pic','wb')
    fd.write(captcha_bin)
    fd.close()
    
    os.system('convert captcha_pic captcha_pic.jpg')
    captcha_string = subprocess.check_output('gocr captcha_pic.jpg', shell=True)
    captcha_string = captcha_string.decode()
    captcha_string = captcha_string.replace('\n','')
    print(captcha_string)
    
    #submitting to the form
    post_tag ={'cametu':captcha_string} 
    #post_tag = urllib.urlencode(post_tag)
    print('submitting the following data:')
    print(post_tag)
    print('\n\n')
    
    post_response = session.post(target_url, data=post_tag)
    if 'retente' in post_response.text:
        print(post_response.text)
        print('bouuuuuuhhhh')
    else:
        success = True
        print(post_response.text)
        print('YEAAAAHHHHHH')

