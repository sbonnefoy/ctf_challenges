import re
import string
import base64
from datetime import timedelta
import sqlite3



def get_command(line):
    command_regexp = re.search(r'(?:order=)(.*)(\sHTTP)',line)

    #retrieving the commands
    if '%3D' in command_regexp[1]:
        command = command_regexp[1].replace('%3D','=')
    else:
        command = command_regexp[1]

    command = base64.b64decode(command).decode()

    return command


def get_timestamp(line):
    timestamp_regexp = re.search(r'(?:2015:)(\d\d:\d\d:\d\d)',line)
    return timestamp_regexp[1]


def get_sleep_time(timestamp1, timestamp2):

        t1 = timestamp1.split(':')
        t2 = timestamp2.split(':')

        time1 = timedelta(int(t1[0]), int(t1[1]), int(t1[2]))
        time2 = timedelta(int(t2[0]), int(t2[1]), int(t2[2]))

        sleep = time2 - time1
        sleep_microseconds = sleep.microseconds
        if sleep_microseconds > 90:
            sleep_microseconds = int(str(sleep_microseconds)[-1])

        return sleep_microseconds



def set_password_list():
    print('Setting password list')
    #char_list = list(string.ascii_uppercase) + \
    #        list(string.ascii_lowercase) + \
    #        ['1','2','3','4','5','6','7','8','9','0','#','-','_',',','.','+','$','*']

    char_list = []
    for i in range(32,127):
        char_list.append(chr(i))
    
    #print(char_list)
    password_list = []
    for i in range(0,21):
        password_list.append(char_list)
    return password_list

def get_password_index(cmd):
    return int(re.search(r'(?:password,)(\d*)', cmd)[1]) -1

def get_substring_index(cmd):
    #retrieve the value of the substring in the sql injection
    match = (re.findall(r'(?:\)\)\),)(\d*)',cmd))

    #pay attention mysql indices start at 1
    sub1 = int(match[0]) - 1

    #some injections are shorter and only have one substring.
    if len(match)<2:
        sub2 = -1
    else:
        sub2 = int(match[1]) - 1

    return sub1, sub2

def long_injection(pass_letter, sub1, sub2):
    ###############################################################
    #compute the result of the function based on the sleep time
    # sleep 1 -> true -> 00
    # sleep 2 -> 2    -> 01
    # sleep 4 -> 3    -> 10
    # sleep 6 -> 4    -> 11
    ###############################################################

    #result = bin(ord(pass_letter)).replace('0b','')[sub1] + bin(ord(pass_letter)).replace('0b','')[sub2]

    #some characters are only encoded on 6 bits. We need to take this into account
    bin_letter = bin(ord(pass_letter))
    #if len(bin_letter) == 8:
    #    result = bin(ord(pass_letter)).replace('0b','0')[sub1] + bin(ord(pass_letter)).replace('0b','0')[sub2]


    #else:
    result = (bin_letter.replace('0b',''))[sub1] + (bin_letter.replace)('0b','')[sub2]

    #this part correspond to the cases checked by the sql injection
    if result == '00' :
        time_sleep=0
    if result == '01':
        time_sleep=2
    if result  == '10':
        time_sleep=4
    if result == '11':
        time_sleep=6
    return time_sleep

def short_injection(pass_letter, sub1):
    #result = bin(ord(pass_letter)).replace('0b','')[sub1]
    bin_letter = bin(ord(pass_letter))

    #print(pass_letter, bin_letter, len(bin_letter))
    #if len(bin_letter) == 8:
    #    bin_letter = bin_letter.replace('0b','0b0')

    #result = bin(ord(pass_letter))[2:][sub1]
    try:
        result = bin(ord(pass_letter)).replace('0b','')[sub1]
    #result = bin(ord(pass_letter)).replace('0b','')[-1]
    #result = bin(ord(pass_letter))[-1]
    #result = bin_letter.replace('0b','')[sub1]
    #result = bin_letter.replace('0b','')[-1]
    

    #this is to remove char whose size does not fit the substring since they
    #are too short
        if result == '0':
            time_sleep = 2

        elif result == '1':
            time_sleep = 4
    except:
        time_sleep = 0

    return time_sleep




def show_password(password):
    print(password)
    for i in password:
        if len(i)!=0:
            print(i[0], end ='', flush=True)
        else:
            print(' ', end='', flush=True)
if __name__ == '__main__':


    password_possibilities = set_password_list()
    command1 = ''
    command2 = ''
    timestamp1 = ''
    timestamp2 = ''

    with open('ch13.txt','r') as f:
        while True:
            #read each line
            line = f.readline()
    
            #if line is empyt we stop reading
            if not line: break
    
            #we need two retrieve two successives command, to get two
            #timestamps and get the sleeping time

            command2 = get_command(line) #command 2 comes afet command 1 in time
            timestamp2 = get_timestamp(line)
        
            if command2 and command1 :
                sleep_time = get_sleep_time(timestamp1, timestamp2)
                #print(sleep_time)

                #retrieve the index of the password and the substring used
                #in the injection
                
                #index of the password used in the injection
                password_index = get_password_index(command1)
                #print(password_index)
            
                #get the substring values used in the injection
                substring_index = get_substring_index(command1)
            
                substring1 = substring_index[0]
                substring2 = substring_index[1]

                #print(substring1, substring2)

                #list to store the letter that fulfill the response
                #on the server for the command
                temp_letter_list = []
            
                #if long injection
                if substring2 >=0 :
                    #loop over the letter list for the password index
                    for letter in password_possibilities[password_index]:
           
                       #check if the sleeping time of the injection corresponds for the 
                       #letter
                       sleep_time_letter = long_injection(letter, substring1, substring2)
                       if sleep_time_letter ==  sleep_time:
                       #if it corresponds, we keep the letter
                          temp_letter_list.append(letter)
                
                #if short injection
                else:
                    if password_index==1:
                        print(password_possibilities[1])
                    #if password_index==1:
                    #    print(password_possibilities[1])
                    for letter in password_possibilities[password_index]:
                        sleep_time_letter = short_injection(letter, substring1)
                        if sleep_time_letter ==  sleep_time or sleep_time == 0:
                            temp_letter_list.append(letter)
            
                #we replace the letter list for the password with the new one
                password_possibilities[password_index] = temp_letter_list
           
            command1 = command2
            timestamp1 = timestamp2
             
    #show the password when finished reading the log file
    show_password(password_possibilities)
    print(password_possibilities[1])

