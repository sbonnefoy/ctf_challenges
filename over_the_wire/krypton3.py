cipher_frequency = 'SQJUBNGCDZWVYMTXKLEAFIOHRP'
#english_frequency = 'ETAOINSHRLDCUMWFGYPBVKJXQZ' 
english_frequency = 'EATSORNIHCDLPUGFWYMBKVJXQZ' 
pass_cipher = 'KSVVWBGSJDSVSISVXBMNYQUUKBNWCUANMJS'

pass_clear = ''
for l in pass_cipher:
    i = cipher_frequency.index(l)
    pass_clear += english_frequency[i]

print(pass_clear)

